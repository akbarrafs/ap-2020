package id.ac.ui.cs.advprog.tutorial5.service;

import id.ac.ui.cs.advprog.tutorial5.repository.SoulRepository;
import id.ac.ui.cs.advprog.tutorial5.core.Soul;

import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class SoulServiceImplTest {

    @Mock
    private SoulRepository soulRepository;

    @InjectMocks
    private SoulServiceImpl soulServiceImpl;

    @Test
    public void whenFindAllIsCalledItShouldCallFindAll(){
        soulServiceImpl.findAll();
        verify(soulRepository, times(1)).findAll();
    }

    @Test
    public void whenFindSoulIsCalledItShouldCallFindById(){
        long longNum = 1;
        soulServiceImpl.findSoul(longNum);
        verify(soulRepository, times(1)).findById(longNum);
    }

    @Test
    public void whenEraseIsCalledItShouldCallDeleteById(){
        long longNum = 1;
        soulServiceImpl.erase(longNum);
        verify(soulRepository, times(1)).deleteById(longNum);
    }

    @Test
    public void whenRewriteIsCalledItShouldCallSave(){
        Soul soul = new Soul(1, "iqbal", 20, "laki", "pelawak");
        soulServiceImpl.rewrite(soul);
        verify(soulRepository, times(1)).save(soul);
    }

    @Test
    public void whenRegisterIsCalledItShouldCallSave(){
        Soul soul = new Soul(1, "fathur", 20, "laki", "pelawak");
        soulServiceImpl.register(soul);
        verify(soulRepository, times(1)).save(soul);
    }
}
