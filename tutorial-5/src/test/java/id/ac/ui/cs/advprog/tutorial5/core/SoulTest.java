package id.ac.ui.cs.advprog.tutorial5.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SoulTest {

    Soul soul;
    @BeforeEach
    public void setUp(){
        soul = new Soul(1, "nama", 10, "gender", "occupation");
    }

    @Test
    public void testGetter(){
        assertEquals(soul.getId(), 1);
        assertEquals(soul.getName(), "nama");
        assertEquals(soul.getAge(), 10);
        assertEquals(soul.getGender(), "gender");
        assertEquals(soul.getOccupation(), "occupation");
    }

    @Test
    public void testSetter(){
        soul.setName("doni");
        soul.setAge(12);
        soul.setGender("laki");
        soul.setOccupation("pelawak");

        assertEquals(soul.getId(), 1);
        assertEquals(soul.getName(), "doni");
        assertEquals(soul.getAge(), 12);
        assertEquals(soul.getGender(), "laki");
        assertEquals(soul.getOccupation(), "pelawak");
    }
}
