package id.ac.ui.cs.advprog.tutorial4.singleton;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.List;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.HolyWish;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class HolyWishTest {

    private Class<?> holyWishClass;
    private HolyWish holyWish;

    @BeforeEach
    public void setUp() throws Exception {
        holyWishClass = Class.forName(HolyWish.class.getName());
        holyWish = HolyWish.getInstance();
    }

    @Test
    public void testNoPublicConstructors() {
        List<Constructor> constructors = Arrays.asList(holyWishClass.getDeclaredConstructors());

        boolean check = constructors.stream()
                .anyMatch(c -> !Modifier.isPrivate(c.getModifiers()));

        assertFalse(!check);
    }


    @Test
    public void testGetInstanceShouldReturnSingletonInstance() {
        HolyWish holyWishTest = HolyWish.getInstance();
        assertEquals(holyWish, holyWishTest);
    }

    @Test
    public void testWish(){
        holyWish.setWish("mikum");
        assertEquals("mikum", holyWish.getWish());
    }

    @Test
    public void testWishExist(){
        HolyWish holyWishTest = HolyWish.getInstance();

        // jika wish 1 di set, seharusnya wish 2 juga di set karena singleton
        holyWish.setWish("mikum");
        assertNotNull(holyWishTest);
        assertEquals("mikum", holyWishTest.getWish());

    }
}
