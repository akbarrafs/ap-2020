package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithGun implements AttackBehavior {
    //ToDo: Complete me
    @Override
    public String attack(){
        return "Suara Menembak: dOr dOr dOr";
    }

    @Override
    public String getType(){
        return "Gun";
    }
}
